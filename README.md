fifteen puzzle for Vr

![Screenshot 1](/screenshots/fifteenvr.png)

Vr Port of fifteen puzzle from the lua / love version from https://simplegametutorials.github.io/love/fifteen/

Here there is another implementation with game instructions: https://www.chiark.greenend.org.uk/~sgtatham/puzzles/js/fifteen.html